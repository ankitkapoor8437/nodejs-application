const express = require("express");
const app = express();

const PORT = 8000;

app.get("/", (req, res) => {
    res.send("This is my node applicaiton for CICD demo!");
});

app.listen(PORT, () => {
    console.log(`App is running on ${PORT}`);
});